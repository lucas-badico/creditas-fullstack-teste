let _ 			= require('lodash'),
	router 		= require('koa-router'),
	views 		= require('co-views'),
	bodyParser 	= require('koa-bodyparser'),
	Auth 		= require('./utils/auth.js'),
    compress  	= require('koa-compress'),
    marko     	= require('marko');
	
	var viewsRouter = router();

	let render = views('./views/');

	// NOTE
	// Need to ready the 
	
	//Redirects to home
	viewsRouter.get('/',function*(){
		this.redirect('/home')
	})
	
	viewsRouter.get('/home',function*(){
		this.body = marko.load('./views/pages/home/template.marko').stream({})
		this.type = 'text/html'
	})
	
	viewsRouter.get('/login',function*(){
//		this.body = require('./views/pages/login/');
		this.body = marko.load('./views/pages/login/template.marko').stream({name:'Lucas',value:3,title:''})
		this.type = 'text/html'
	})
	
	viewsRouter.get('/register',function*(){
		this.body = marko.load('./views/pages/register/template.marko').stream({})
		this.type = 'text/html'
	})
	
	viewsRouter.post('/tryPost', function*(){
		console.log(this.request.body);
		this.body = this.request.body;
	})
	
	//home is set to auth validation
	viewsRouter.use('/home',Auth.isAuth(),Auth.handle());


module.exports = viewsRouter;