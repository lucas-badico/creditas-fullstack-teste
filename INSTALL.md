## 1. Preparação do ambiente
	* rethinkdb server - https://www.rethinkdb.com/docs/install/
	* nodejs e npm
	* rethink-migrate - npm install -g rethink-migrate
	* inicie o rethinkdb
	
## 2. Download o repositório
	https://bitbucket.org/lucas-badico/creditas-fullstack-teste
	
## 3. Instale os pacotes
	cmd:
	npm install
	
## 4. Migre a database
	cmd:
	rethink-migrate up
	
## 5. Test o ambiente
	cmd:
	npm test
	
	Isso irá testar:
	- Components
		- users
		- sessoes
		- messages
	- Endpoints
		- acesso componentes
	- Views
	- Widgets

## 6. Inicie o app
	cmd:
	npm start