## Teste frontend?Fullstack

Bom pessoal decidi expandir um pouco o escopo do desafio, e escrever um backend simples usando koajs. Eu já tinha pronto aqui um ambiente com usuarios e sessao, usando rethinkdb como database. 

O que vou criar especificamente para o teste é o componente de chat/messages. A view e interface de chat, como o teste original pede. 

NOTA 1: O arquivo de readme original esta renomeado como CHALENGE.

NOTA 2: Como estou usando esse documento para defender minhas escolhas, vou colocar o passo a passo para instalar e rodar o projeto no arquivo INSTALL.

### Ferramentas
- koaJS - Webframework que trabalha com funções geradoras no NodeJS.
- rethinkdb - database com recurso de realtime listning.
- LassoJS - Um Client-side Bundler com alguns recursos interessantíssimos.
- MarkoJS - Template engine da equipe do ebay, com recursos assincronos.
- Marko-Widgets - Uma extensão do Makojs, que usa um pouco da filosofia do Reactjs/redux para fornecer aos components comunicação entre front e back.
- Nas views vou usar um template de site de viagens que ando usando no meu projeto atual. Não vi nesse template, nem vou procurar, algo como um chat, para poder criar ele apartir das guidelines desse projeto.

Meus principais desafios vão ser:

- Markojs widgets: quero usar eles embutidos com a logica, então, o componente de login que tem o recurso de comunicar com o endpoint correto, não importa de onde eu chame ele.(e especificamente isso eu não tenho pronto ainda).

- Quero já deixar os componentes de mensagem, enviar mensagem, e caixa de texto de mensagem prontos para expandirmos suas funcionalidades dentro dos widgets e assim eles atualizarem em todos os lugares que viermos a usar.

### Estimativa de tempo
Estou começando esse repo no exato momento em que enviei  email para o leornardo. Estimo que levarei o dia para concluir tudo isso que eu me propus a fazer.

	OPS! estimei errado, criei um planejamento mais preciso.

Planejamento:
Sexta - 17/fev
 3 horas
 - Basic structure
 - specs
 - docs

Sábado - 18/fev
3 horas
 - routes
 - auth handles
 - marko widgets & lasso.js tryout

Domingo - 19/fev
 3 horas
 - login widget
 - register widget
 
Segunda - 20/fev
3 horas
 - messages widget
 - message infra


### Estrutura

#### Componentização
O componente a que me refiro são as entidades da logica do negocio sem relação com as views.
Organizo a minha estrutura por componentes, que seriam o controller + model, e sempre ao lado o teste.
Estou usando mocha, com auxilio do supertest para manter tudo no lugar.

*Os componentes visuais eu vou chamar de widgets(agradeço ao time do markojs por me livrar dessa ambiguação)*

### Router e mount
Gosto de separar a logica dos componentes em um app distinto do app principal. Evita acoplamentos desnecessários, para quando for necessário, por exemplo, mandar a api para um app totalmente separado do app que renderiza as views não ser necessario reescrever tanta coisa.

*Penso que isso é algo comum hoje, se por exemplo esse chat for ser usado em um mobile app. O cenario ideal é ter essa camada de api com um time distindo daquele que cuida do webapp.*

### Public e views
Estou usando a */public* para servir os elementos estaticos, js, css e imagens. E a views é onde estarão os meus templates e inclusive os widgets.

#### Pages
São apenas 3 páginas. Home que tras a interface de trocas de mensagens e login e registro. Para montar essas 3 utilizamos dos layouts padrões que são automaticamente compilados pelo lassojs e o marko templates montados com os widgets

- [ ] Páginas
	- [ ] Login
	- [ ] Registro
	- [ ] Home/mensagens
- [x] Layout Basico
	
#### Widgets
Ou UI Componentes

- [ ] Logo chama o tipo do logo e  ação ao click
- [ ] Login/Registro
	- [ ] Login
	- [ ] Registro
	- [ ] botão enviar dados
	- [ ] botão trocar modo (login/registro)

- [ ] Mensagens
	- [ ] 	ContactList
	- [ ] 		feedBody
	- [ ] 		inputBox
	- [ ] 		sendButtom