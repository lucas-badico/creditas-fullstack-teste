// Compiled using marko@4.0.0-rc.18 - DO NOT EDIT
var marko_template = module.exports = require("marko/html").t(__filename),
    marko_helpers = require("marko/runtime/html/helpers"),
    marko_loadTag = marko_helpers.t,
    include_tag = marko_loadTag(require("marko/taglibs/core/include-tag")),
    lasso_head_tag = marko_loadTag(require("lasso/taglib/head-tag")),
    init_widgets_tag = marko_loadTag(require("marko/widgets/taglib/init-widgets-tag")),
    lasso_body_tag = marko_loadTag(require("lasso/taglib/body-tag")),
    browser_refresh_tag = marko_loadTag(require("browser-refresh-taglib/refresh-tag")),
    await_reorderer_tag = marko_loadTag(require("marko/taglibs/async/await-reorderer-tag"));

function render(input, out) {
  var data = input;

  out.w("<!doctype html><html lang=\"en\"><head><meta charset=\"UTF-8\"><title>");

  include_tag({
      _target: data.title
    }, out);

  out.w("</title><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><meta name=\"keywords\" content=\"template, tour template, city tours, city tour, tours tickets, transfers, travel, travel template\"><meta name=\"description\" content=\"Full Stack Developer Teste | Lucas Badico para o Creditas\"><meta name=\"author\" content=\"Ansonika\"><title>CITY TOURS - City tours and travel site template by Ansonika</title><link rel=\"shortcut icon\" href=\"img/favicon.ico?v=2\" type=\"image/x-icon\"><link rel=\"apple-touch-icon\" type=\"image/x-icon\" href=\"img/apple-touch-icon-57x57-precomposed.png\"><link rel=\"apple-touch-icon\" type=\"image/x-icon\" sizes=\"72x72\" href=\"img/apple-touch-icon-72x72-precomposed.png\"><link rel=\"apple-touch-icon\" type=\"image/x-icon\" sizes=\"114x114\" href=\"img/apple-touch-icon-114x114-precomposed.png\"><link rel=\"apple-touch-icon\" type=\"image/x-icon\" sizes=\"144x144\" href=\"img/apple-touch-icon-144x144-precomposed.png\"><link href=\"css/base.css\" rel=\"stylesheet\"><link href=\"css/flickity.css\" rel=\"stylesheet\"><link href=\"http://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\"><link href=\"http://fonts.googleapis.com/css?family=Gochi+Hand\" rel=\"stylesheet\" type=\"text/css\"><link href=\"http://fonts.googleapis.com/css?family=Lato:300,400\" rel=\"stylesheet\" type=\"text/css\"><!--[if lt IE 9]>\n      <script src=\"js/html5shiv.min.js\"></script>\n      <script src=\"js/respond.min.js\"></script>\n    <![endif]-->");

  lasso_head_tag({}, out);

  out.w("</head><body><!--[if lte IE 8]>\n    <p class=\"chromeframe\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a>.</p>\n<![endif]--><div id=\"preloader\"><div class=\"sk-spinner sk-spinner-wave\"><div class=\"sk-rect1\"></div><div class=\"sk-rect2\"></div><div class=\"sk-rect3\"></div><div class=\"sk-rect4\"></div><div class=\"sk-rect5\"></div></div></div><div class=\"layer\"></div><header><div class=\"container\"><div class=\"row\"><div class=\"col-md-3 col-sm-3 col-xs-3\"><div id=\"logo\"><a href=\"index.html\"><img src=\"img/logo.png\" width=\"160\" height=\"34\" alt=\"City tours\" data-retina=\"true\" class=\"logo_normal\"></a><a href=\"index.html\"><img src=\"img/logo_sticky.png\" width=\"160\" height=\"34\" alt=\"City tours\" data-retina=\"true\" class=\"logo_sticky\"></a></div></div></div></div></header>");

  include_tag({
      _target: data.body
    }, out);

  out.w("<footer><div class=\"container\"><div class=\"row\"><div class=\"col-md-5 col-sm-4\"><h3>Que tal batermos um papo?</h3><a href=\"tel://941738544\" id=\"phone\">+55 941 738 544</a><a href=\"mailto:lucasbadico@gmail.com\" id=\"email_footer\">lucasbadico@gmail.com</a></div><div class=\"col-md-3 col-sm-4\"><h3>Sobre</h3><ul><li><a href=\"/breve\" disabled>Sobre mim</a></li><li><a href=\"/breve\" disabled>FAQ</a></li><li><a href=\"/login\">Login</a></li><li><a href=\"/register\">Register</a></li></ul></div><div class=\"col-md-3 col-sm-4\"><h3>Me Descubra</h3><ul><li><a href=\"https://www.linkedin.com/in/lucas-gomes-9b08511b\" target=\"_blank\">Linkedin</a></li><li><a href=\"https://bitbucket.org/lucas-badico\" target=\"_blank\">Bitbucket</a></li><li><a href=\"http://trampos.co/lucasbadico\" target=\"_blank\">Currículo</a></li><li><a href=\"https://bitbucket.org/lucas-badico/creditas-fullstack-teste\">Source Code desse projeto</a></li></ul></div></div><div class=\"row\"><div class=\"col-md-12\"><div id=\"social_footer\"><p>© Badico 2017</p></div></div></div></div></footer>");

  init_widgets_tag({}, out);

  out.w("<div id=\"toTop\"></div><script src=\"js/jquery-1.11.2.min.js\"></script><script src=\"js/common_scripts_min.js\"></script><script src=\"js/functions.js\"></script>");

  lasso_body_tag({}, out);

  browser_refresh_tag({
      enabled: true
    }, out);

  await_reorderer_tag({}, out);

  init_widgets_tag({}, out);

  out.w("</body></html>");
}

marko_template._ = render;

marko_template.meta = {
    deps: [
      "./style.less"
    ],
    tags: [
      "marko/taglibs/core/include-tag",
      "lasso/taglib/head-tag",
      "marko/widgets/taglib/init-widgets-tag",
      "lasso/taglib/body-tag",
      "browser-refresh-taglib/refresh-tag",
      "marko/taglibs/async/await-reorderer-tag"
    ]
  };
